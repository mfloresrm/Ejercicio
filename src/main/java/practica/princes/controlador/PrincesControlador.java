package practica.princes.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import practica.princes.entidad.Princes;
import practica.princes.servicios.PrincesServicios;

@RestController
@RequestMapping("/comercio-electronico")
public class PrincesControlador {
	@Autowired
	private PrincesServicios princesServicio;
	
	

	@PostMapping("/ingresar-nuevo-producto")
	public ResponseEntity<Princes> save(@RequestBody Princes princes){
	return new ResponseEntity<>(princesServicio.save(princes), HttpStatus.CREATED);

	}

	
	@GetMapping("/ver-productos")
	public ResponseEntity<List<Princes>> findAll(){
	return new ResponseEntity<>(princesServicio.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/ver-productos/{id}")
	public ResponseEntity<Princes> findById(@PathVariable Integer id)
	{
	return new ResponseEntity<>(princesServicio.getId(id), HttpStatus.OK);
	}
	
}
