package practica.princes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrincesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrincesApplication.class, args);
				
	}

}
