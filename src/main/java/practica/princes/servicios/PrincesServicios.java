package practica.princes.servicios;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import practica.princes.entidad.Princes;
import practica.princes.repositorio.PrincesRepositorio;


@Transactional
@Service
public class PrincesServicios {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private PrincesRepositorio princesRepositorio;

	public List<Princes> findAll(){
	return princesRepositorio.findAll();
	}
	
	public Princes save(Princes princes){
		 return princesRepositorio.save(princes);
		}
	
	public Princes getId(Integer id){
		return princesRepositorio.getById(id);
		}
	
	public long insert(Princes princes){
		entityManager.persist(princes);
		return princes.getBrandId();
	}
	

	
	
}
