package practica.princes.repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import practica.princes.entidad.Princes; 

@Repository
public interface PrincesRepositorio extends JpaRepository<Princes, Integer>{

}
