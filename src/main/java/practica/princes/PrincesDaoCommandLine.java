package practica.princes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import practica.princes.entidad.Princes;
import practica.princes.servicios.PrincesServicios;


@Component
public class PrincesDaoCommandLine implements CommandLineRunner{
	
	@Autowired
	private PrincesServicios princesServicio;
	
	@Override
	public void run(String... args) throws Exception {
		
		//Insercion de datos en H2
		Princes prince = new Princes(1, "2020-06-14-00.00.00", "2020-12-31-23.59.59", 1, 35455, 0, 35.50, "EUR");
		Princes prince2 = new Princes(1, "2020-06-14-15.00.00", "2020-06-14-18.30.00", 2, 35455, 1, 25.45, "EUR");
		Princes prince3 = new Princes(1, "2020-06-15-00.00.00", "2020-06-15-11.00.00", 3, 35455, 1, 30.50, "EUR");
		Princes prince4 = new Princes(1, "2020-06-15-16.00.00", "2020-12-31-23.59.59", 4, 35455, 1, 38.95, "EUR");
		princesServicio.insert(prince);
		princesServicio.insert(prince2);
		princesServicio.insert(prince3);
     	princesServicio.insert(prince4);
	
	}
}
